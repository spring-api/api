package com.carloscalgaro.spring_api.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name="institution")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Institution {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(length=30)
	@NotNull(message="Não pode ser Nulo")
	@NotEmpty(message="Não pode estar vazio")
	private String nome;
	
	@Column(length=100)
    @NotNull(message="Não pode ficar em branco")
    @NotEmpty(message="Não pode ficar vazio")
	private String endereco;

	public Institution(){
	    super();
    }
	public Institution(Long id, String nome, String endereco) {
		super();
	    this.id = id;
		this.nome = nome;
		this.endereco = endereco;
	}

	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="city_id", nullable = true)
    @JsonManagedReference
    private City city;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public void setId(Long id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getEndereco() {
		return endereco;
	}

}
