package com.carloscalgaro.spring_api.repositories;

import com.carloscalgaro.spring_api.models.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Long> {

}

