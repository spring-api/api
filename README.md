# Sample API using Spring MVC

This is a sample API implemented using Spring MVC and Hibernate


---
Database Setup
---

All config is done in the _application.properties_ file located in _src/main/resources_

Just make sure you create the database schema manually

---
Interesting Links
---

- http://websystique.com/springmvc/spring-mvc-4-restful-web-services-crud-example-resttemplate/
