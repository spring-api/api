package com.carloscalgaro.spring_api.controllers;

import com.carloscalgaro.spring_api.models.City;
import com.carloscalgaro.spring_api.repositories.CityRepository;
import com.carloscalgaro.spring_api.repositories.InstitutionRepository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cities")
public class CitiesController {

    @Autowired
    private CityRepository cityRepository;
    private ObjectMapper mapper = new ObjectMapper();

    /**
     * GET
     * */
    @GetMapping(value = {"", "/all"}, produces = "application/json")
    public ResponseEntity<List<City>> all(){
        List<City> cities = cityRepository.findAll();
        return new ResponseEntity<List<City>>(cities, HttpStatus.OK);
    }

    /**
     * GET(ID)
     * */
    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<?> get(@PathVariable long id) {
        Optional<City> city = cityRepository.findById(id);
        if(city.isPresent())
            return new ResponseEntity<String>("",HttpStatus.OK);
        return new ResponseEntity<City>(city.get(), HttpStatus.OK);
    }

    /**
     * CREATE
     * */
    @PostMapping(value = "/create", produces = "application/json")
    public ResponseEntity<?> create(@ModelAttribute("city") @Valid City city,
                         BindingResult result) throws JsonProcessingException {
        if (result.hasErrors()) {
           return new ResponseEntity<String>(mapper.writeValueAsString(result.getAllErrors()), HttpStatus.NOT_FOUND);
        } else {
            cityRepository.save(city);
            return new ResponseEntity<City>(city, HttpStatus.OK);
        }
    }

    /**
     * UPDATE
     * */
    @RequestMapping(value="update/{id}", method = RequestMethod.PUT, produces="applications/json")
    public ResponseEntity<?> update(@ModelAttribute("city") @Valid City city, BindingResult result) throws JsonProcessingException {
        if(result.hasErrors())
            return new ResponseEntity<String>(mapper.writeValueAsString(result.getAllErrors()), HttpStatus.BAD_REQUEST);
        cityRepository.save(city);
        return new ResponseEntity<City>(city, HttpStatus.OK);
    }

    @RequestMapping(value="delete/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<City> delete(@PathVariable("id") Long id){
        cityRepository.deleteById(id);
        return new ResponseEntity<City>(HttpStatus.OK);
    }
}

