package com.carloscalgaro.spring_api.controllers;

import com.carloscalgaro.spring_api.models.Institution;
import com.carloscalgaro.spring_api.repositories.CityRepository;
import com.carloscalgaro.spring_api.repositories.InstitutionRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/institutions")
public class InstitutionsController{

    @Autowired
    private CityRepository cityRepository;
    @Autowired
    private InstitutionRepository institutionRepository;

    private ObjectMapper mapper = new ObjectMapper();

    /**
     * GET
     * */
    @GetMapping(value = {"", "/all"}, produces = "application/json")
    public @ResponseBody ResponseEntity<List<Institution>> all(){
        List<Institution> institutions = institutionRepository.findAll();
        if(institutions.isEmpty()){
            return new ResponseEntity<List<Institution>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<Institution>>(institutions, HttpStatus.OK);
    }


    /**
     * GET(ID)
     * */
    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Institution> get(@PathVariable("id") long id) {
        Institution institution= institutionRepository.getOne(id);
        if(institution == null){
            return new ResponseEntity<Institution>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Institution>(institution, HttpStatus.OK);
    }

    /**
     * CREATE
     * */
    @PostMapping(value = {"", "/create"}, produces = "application/json")
    public ResponseEntity<?> create(@ModelAttribute("institution") @Valid Institution institution,
                            BindingResult result, Model model) throws JsonProcessingException {
        if (result.hasErrors()) {
            return new ResponseEntity<String>(mapper.writeValueAsString(result.getAllErrors()), HttpStatus.BAD_REQUEST);
        } else {
            institutionRepository.save(institution);
            return new ResponseEntity<Institution>(institution,HttpStatus.OK);
        }
    }

    /**
     * UPDATE
     * */
    @RequestMapping(value="update/{id}", method = RequestMethod.PUT, produces="applications/json")
    public ResponseEntity<?> update(@ModelAttribute("institution") @Valid Institution institution, BindingResult result) throws JsonProcessingException {
        if(result.hasErrors())
            return new ResponseEntity<String>(mapper.writeValueAsString(result.getAllErrors()), HttpStatus.BAD_REQUEST);
        institutionRepository.save(institution);
        return new ResponseEntity<Institution>(institution, HttpStatus.OK);
    }

    @RequestMapping(value="delete/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> delete(@PathVariable("id") Long id){
        Institution institution = institutionRepository.getOne(id);
        institution.setCity(null);
        institutionRepository.delete(institution);
        return new ResponseEntity<String>( "ok", HttpStatus.OK);
    }

}
