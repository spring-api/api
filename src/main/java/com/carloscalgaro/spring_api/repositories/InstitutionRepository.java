package com.carloscalgaro.spring_api.repositories;

import com.carloscalgaro.spring_api.models.Institution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InstitutionRepository extends JpaRepository<Institution, Long> {

    public Institution findByNome(String nome);

    @Query("SELECT i FROM Institution i WHERE i.nome LIKE %:nome%")
    public List<Institution> searchByName(@Param("nome") String nome);
}
